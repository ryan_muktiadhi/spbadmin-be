Setup Database KRAKATOA
	-Migrasi Table : 
		-Buka file knex.js di folder connection
		-Sesuaikan koneksi yang dituju (contoh ada di dbORM.js)
		-Masuk ke ke folder migration dengan cmd
		-Jalankan command :
			npx knex migrate:latest --knexfile ../knexfile.js
	-Setup Data inisial:
		-Buka file knex.js di folder connection
		-Sesuaikan koneksi yang dituju (contoh ada di dbORM.js)
		-Masuk ke ke folder migration dengan cmd
		-Jalankan command :
			npx knex seed:run
	-Setup Koneksi db :
		-Buka file knex.js di folder connection
		-Sesuaikan koneksi yang dituju
		

Setup Database KOMI PORTAL
	-Migrasi Table : 
		-Buka file knex.js di folder connection
		-Sesuaikan koneksi yang dituju (contoh ada di dbORM.js)
		-Masuk ke ke folder migration dengan cmd
		-Jalankan command :
			npx knex migrate:latest --knexfile ../knexfile.js
	-Setup Koneksi db :
		-Buka file config.js
		-Jika ingin menggunakan pada bagian db dan dbkkt masukan salah satu dari:
			- oracle: untuk db oracle
			- postgres : untuk db postgre
		-pada dbconfig: isi hostname,username, password dan database
	-Settingan config.js
		-kktserver: service kraktoa yang berjalan
		-port: untuk setup port yang akan digunakan komi

Setup Oracle :
	-Linux:
		-copy file instantclient_21_4 ke server
		-unzip file tersebut
		-kemudian jalankan command :
			export LD_LIBRARY_PATH=<lokaski_file_instantclient_21_4>/instantclient_21_4:$LD_LIBRARY_PATH