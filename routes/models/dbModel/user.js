//const knex = require("../../../connection/ormSqlServer");
const knex = require("../../../connection/ormOracle");

const createUserTable = async () => {
  return knex.schema.createTable("persons", function (table) {
    table.increments("id").primary();
    table.string("name").notNullable();
  });
};

module.exports = { createUserTable };
