const pool = require("../../../connection/db");

const insertGroup = async (dataobject, apps, idTenant, grouptype) => {
  console.log("INi idTenant", idTenant);
  const { group, modules, menus } = dataobject;
  // const groupResult = await pool.query(
  //   "INSERT INTO public.mst_usergroup " +
  //     " ( groupname, idtenant, created_byid, created_at, active, idapplication) " +
  //     "VALUES ( $1, $2, $3, to_timestamp($4 / 1000.0), $5,$6) returning *",
  //   [group.groupName, idTenant, apps.id, Date.now(), 1, apps.id]
  // );
  const groupResult = await pool.query(
    "INSERT INTO public.mst_usergroup " +
      " ( groupname, idtenant, created_byid, created_at, active, idapplication, grouptype) " +
      "VALUES ( $1, $2, $3, to_timestamp($4 / 1000.0), $5,$6,$7) returning *",
    [
      group.groupName,
      idTenant,
      apps.id,
      Date.now(),
      1,
      apps.id,
      group.groupType,
    ]
  );
  let modulesResult;
  if (groupResult.rows.length > 0 && modules.length > 0) {
    const modulesResult = await Promise.all(
      modules.map(async (module) => {
        //console.log(module);
        try {
          moduleData = await pool.query(
            "INSERT INTO public.mst_usergroupacl " +
              "( idgroup, idtenant, idmodule, " +
              "  fview, created_byid, created_at) " +
              " VALUES ( $1,$2,$3,$4,$5,to_timestamp($6 / 1000.0)) RETURNING *",
            [
              groupResult.rows[0].id,
              idTenant,
              // module.moduleId,
              module.id,
              module.view,
              apps.id,
              Date.now(),
            ]
          );
          // module.acl.view,

        } catch (err) {
          console.log(err);
          return 0;
        }
        return moduleData;
      })
    );
    if (modulesResult[0].rows.length > 0 && menus.length > 0) {
      try {
        let menuResult;
        menuResult = await Promise.all(
          menus.map(async (menu) => {
            return await pool.query(
              "INSERT INTO public.mst_usergroupacl " +
                "( idgroup, idtenant, idmodule, idmenu, " +
                " fcreate, fread, fupdate, fdelete," +
                "  fview, created_byid, created_at) " +
                " VALUES ( $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,to_timestamp($11 / 1000.0)) RETURNING *",
              [
                groupResult.rows[0].id,
                idTenant,
                menu.moduleId,
                menu.menuId,
                menu.acl.create,
                menu.acl.read,
                menu.acl.update,
                menu.acl.delete,
                menu.acl.view,
                apps.id,
                Date.now(),
              ]
            );
          })
        );
        console.log(menuResult[0]);
        if (menuResult[0].rows.length > 0) {
          return 1;
        } else {
          return 0;
        }
      } catch (err) {
        console.log(err);
      }
    }
  } else {
    return 0;
  }
};

const deleteGroup = async (dataobject, apps) => {
  const { group, modules, menus } = dataobject;
  try {
    let groupResult = await pool.query(
      "Delete from public.mst_usergroup where id =  $1",
      [group.id]
    );
    let moduleResult = await pool.query(
      "Delete from public.mst_usergroupacl where idgroup =  $1",
      [group.id]
    );
    return 1;
  } catch (err) {
    console.log(err);
    return 0;
  }
};

const editGroup = async (group, apps, idTenant) => {
  try {
    let groupResult = await pool.query(
      "UPDATE public.mst_usergroup set groupname = $1,updated_at = to_timestamp($2 / 1000.0) where id =  $3 returning *",
      [group.groupName, Date.now(), group.groupId]
    );
    return groupResult.rows[0].id;
  } catch (err) {
    console.log(err);

    return 0;
  }
};

const editModules = async (modules, groupId, apps, idTenant, grouptype) => {
  try {
    await pool.query(
      "Delete   FROM public.mst_usergroupacl where  idgroup= $1 and idmodule is not null",
      [groupId]
    );

    await modules.map(async (module) => {
      moduleData = await pool.query(
        "INSERT INTO public.mst_usergroupacl ( idgroup, idtenant, idmodule, fview, created_byid, created_at) VALUES ( $1,$2,$3,$4,$5,to_timestamp($6 / 1000.0)) RETURNING *",
        [
          groupId,
          idTenant,
          module.moduleId,
          module.acl.view,
          apps.id,
          Date.now(),
        ]
      );
    });

    return 1;
  } catch (err) {
    console.log(err);

    return 0;
  }
};

const editMenus = async (menus, groupId, apps, idTenant, grouptype) => {
  try {
    let moduleResult = await pool.query(
      "Delete  FROM public.mst_usergroupacl where idgroup =$1 and idmenu is not null",
      [groupId]
    );
    await menus.map(async (menu) => {
      await pool.query(
        "INSERT INTO public.mst_usergroupacl ( idgroup, idtenant, idmodule, idmenu,  fcreate, fread, fupdate, fdelete, fview, created_byid, created_at) VALUES ( $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,to_timestamp($11 / 1000.0)) RETURNING *",
        [
          groupId,
          idTenant,
          menu.moduleId,
          menu.menuId,
          menu.acl.create,
          menu.acl.read,
          menu.acl.update,
          menu.acl.delete,
          menu.acl.view,
          apps.id,
          Date.now(),
        ]
      );
    });

    return 1;
  } catch (err) {
    console.log(err);

    return 0;
  }
};

module.exports = {
  insertGroup,
  deleteGroup,
  editGroup,
  editModules,
  editMenus,
};
