const express = require("express"),
  app = express();
const router = express.Router();
const jwttools = require("../../routes/services/utils/encryptdecryptjwt");
const groupUtils = require("./utils/groupUtils");
const pool = require("../../connection/db");
const checkAuth = require("../../middleware/check-auth");
const moment = require("moment");
// const axios = require('axios')
router.use(checkAuth);
const knex = require("../../connection/dborm");
var conf = require("../../config.json");
const conn = knex.conn();


router.get("/getAllacc", async (req, res, next) => {
    const authHeader = req.headers.authorization;
    var dcodeInfo = req.userData;
    // var apps = dcodeInfo.apps[0];
    var idtenant = dcodeInfo.idtenant;
    try {
   
      const accAdmins = await conn
        .select(
            "id", 
            "account", 
            "accname", 
            "bankname", 
            "balance", 
            "created_by", 
            "created_at", 
            "updated_at", 
            "active", 
            "idapproval"
        )
        .from("rekeningbank")
        .orderBy("created_at", "desc");
  
      if (accAdmins.length > 0) {
        // setTimeout(function () {
          res.status(200).json({ status: 200, data: accAdmins });
        // }, 500);
      } else {
        setTimeout(function () {
          res.status(200).json({ status: 202, data: accAdmins });
        }, 500);
      }
    } catch (err) {
      console.log(err);
      res.status(500).json({ status: 500, data: "internal error" });
    }
  });
  router.get("/getacc/:id", async (req, res, next) => {
    const authHeader = req.headers.authorization;
    var dcodeInfo = req.userData;
    // var apps = dcodeInfo.apps[0];
    try {
      let idacc = req.params.id;
      // console.log(">>>>>>>>>>>> Tenanr, Level "+JSON.stringify(dcodeInfo.leveltenant));
      //   console.log(dcodeInfo.leveltenant);TO_CHAR(clis.expiredate,'yyyy-MM-dd')
  
      // var query = "SELECT  bank_code id, bank_code, acc_code, bank_name, last_update_date, TO_CHAR(created_date,'yyyy-MM-dd HH:mm:ss') created_date, change_who, idtenant FROM m_acc WHERE bank_code=$1 order by created_date desc";
      // const accAdmins = await pool.query(
      //   query, [bankcode]);
  
      const accAdmins = await conn
        .select(
          "id", 
            "account", 
            "accname", 
            "bankname", 
            "balance", 
            "created_by", 
            "created_at", 
            "updated_at", 
            "active", 
            "idapproval"
        )
        .from("rekeningbank")
        .where("id", idacc)
      if (accAdmins.length > 0) {
        setTimeout(function () {
          res
            .status(200)
            .json({ status: 200, data: { accAdmins: accAdmins[0] } });
        }, 500);
      } else {
        setTimeout(function () {
          res.status(200).json({ status: 202, data: "Data tidak ditemukan" });
        }, 500);
      }
    } catch (err) {
      console.log(err);
      res.status(500).json({ status: 500, data: "internal error" });
    }
  });
  router.post("/insertacc", async (req, res, next) => {
    var dcodeInfo = req.userData;
    const change_who = dcodeInfo.id;
    const idtenant = dcodeInfo.idtenant;
    try {
      // var apps = dcodeInfo.apps[0];
      // SELECT id, account, accname, bankname, balance, created_by, created_at, updated_at, active, idapproval
      const { account, accname, bankname, balance } = req.body;
      // const resp = await pool.query(
      //   "INSERT INTO m_acc (bank_code, acc_code, bank_name, change_who, idtenant) VALUES ($1, $2, $3, $4, $5) ",
      //   [
      //     bank_code, acc_code, bank_name, dcodeInfo.id, dcodeInfo.idtenant
      //   ]
      // );
      const resp = await conn("rekeningbank")
        .insert({
          account: account,
          accname: accname,
          bankname: bankname,
          balance: balance,
          active: 3,
        })
        .returning(["accname"]);
      console.log(resp);
      if (resp.length > 0) {
        res.status(200).json({ status: 200, data: resp });
      } else {
        res.status(500).json({
          status: 500,
          data: "Error insert m_acc ",
        });
      }
    } catch (err) {
      console.log(err);
      res.status(500).json({ status: 500, data: "Error insert User" });
    }
  });
  
  router.post("/updateacc", async (req, res, next) => {
    var dcodeInfo = req.userData;
    try {
      // var apps = dcodeInfo.apps[0];
      const { id, accname, account, bankname, balance } = req.body;
      const today = moment().format("YYYY-MM-DD HH:mm:ss");
      // payload = {
      //   id: this.accId,
      //   accname: this.groupForm.get('accname')?.value,
      //   account: this.groupForm.get('account')?.value,
      //   bankname: this.groupForm.get('bankname')?.value,
      //   balance: this.groupForm.get('balance')?.value,
      // };
      // const resp = await pool.query(
      //   "UPDATE m_acc SET bank_code=$1, acc_code=$2, bank_name=$3, last_update_date=$4 WHERE acc_code=$5 ",
      //   [
      //     bank_code, acc_code, bank_name, today, old_code
      //   ]
      // );
      const resp = await conn("rekeningbank").where("id", id).update({
        account: account,
        accname: accname,
        bankname: bankname,
        balance: balance,
        updated_at : today
      });
      if (resp > 0) {
        res.status(200).json({ status: 200, data: resp });
      } else {
        res.status(500).json({
          status: 500,
          data: "Error insert m_acc ",
        });
      }
    } catch (err) {
      console.log(err);
      res.status(500).json({ status: 500, data: "Error insert User" });
    }
  });
  router.post("/updateaccapprove", async (req, res, next) => {
    var dcodeInfo = req.userData;
    try {
      const { id, oldactive, isactive, idapproval } = req.body;
      const today = moment().format("YYYY-MM-DD HH:mm:ss");
      const resp = await conn("rekeningbank").where("id", id).update({
        active:isactive,
        updated_at:today
      });
      if (resp > 0) {
        res.status(200).json({ status: 200, data: resp });
      } else {
        res.status(500).json({
          status: 500,
          data: "Error insert m_acc ",
        });
      }
    } catch (err) {
      console.log(err);
      res.status(500).json({ status: 500, data: "Error insert User" });
    }
  });
  router.get("/deleteacc/:id", async (req, res, next) => {
    // const authHeader = req.headers.authorization;
    console.log("################## >>>> ##################################");
    var dcodeInfo = req.userData;
    // var apps = dcodeInfo.apps[0];
    let idacc = req.params.id;
    console.log("### Bank Code ### " + idacc);
    try {
      // let resp = await pool.query(
      //   "DELETE FROM m_acc where bank_code = $1",
      //   [bankcode]
      // );
      let resp = await conn("rekeningbank").where("id", idacc).del();
      res.status(200).json({ status: 200, data: "Success" });
    } catch (err) {
      console.log(err);
      res.status(500).json({ status: 500, data: "Error retrive Users" });
    }
  });
  
  module.exports = router;