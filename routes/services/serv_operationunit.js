const express = require("express"),
  app = express();
const router = express.Router();
const jwttools = require("../../routes/services/utils/encryptdecryptjwt");
const groupUtils = require("./utils/groupUtils");
const pool = require("../../connection/db");
const checkAuth = require("../../middleware/check-auth");
const moment = require("moment");
// const axios = require('axios')
router.use(checkAuth);
const knex = require("../../connection/dborm");
var conf = require("../../config.json");
const conn = knex.conn();

router.get("/getdbinstancebytnname/:cname", async (req, res, next) => {
    const authHeader = req.headers.authorization;
    var dcodeInfo = req.userData;
    var apps = dcodeInfo.apps[0];
    let cname = req.params.cname;
    try {
      const allInstance = await conn
        .select(
          "DB_INSTANCE", 
            "TYPE", 
            "NAME",  
            "id", 
            "idtenant"
        )
        .from("lov")
        .where("NAME", cname);
      if (allInstance.length > 0) {
          res
            .status(200)
            .json({ status: 200, data: allInstance[0] });
       
      } else {
        setTimeout(function () {
          res.status(200).json({ status: 202, data: "Data tidak ditemukan" });
        }, 500);
      }
    } catch (err) {
      console.log(err);
      res.status(500).json({ status: 500, data: "internal error" });
    }
});

 
  
  module.exports = router;