const express = require("express"),
  app = express();
const router = express.Router();
const jwttools = require("../../routes/services/utils/encryptdecryptjwt");
const groupUtils = require("./utils/groupUtils");
const pool = require("../../connection/db");
const checkAuth = require("../../middleware/check-auth");
const moment = require("moment");
// const axios = require('axios')
router.use(checkAuth);
const knex = require("../../connection/dborm");
var conf = require("../../config.json");
const conn = knex.conn();

router.get("/getallusermappings", async (req, res, next) => {
    const authHeader = req.headers.authorization;
    var dcodeInfo = req.userData;
    var apps = dcodeInfo.apps[0];
    let cname = req.params.cname;
    try {
      const allInstance = await conn
        .select(
          "id", 
            "idusrkkt", 
            "usrkktname",  
            "dbinstance", 
            "vendorid",
            "vendorname",
            "orgid",
            "idtenant",
            "targetemail1",
            "targetemail2",
            "idtenantuser",
            "created_at",
            "updated_at",
            "created_by",
            "companyname",
            "companycode"
        )
        .from("spb_usermapping");
      if (allInstance.length > 0) {
          res
            .status(200)
            .json({ status: 200, data: allModules[0] });
       
      } else {
        setTimeout(function () {
          res.status(200).json({ status: 202, data: "Data tidak ditemukan" });
        }, 500);
      }
    } catch (err) {
      console.log(err);
      res.status(500).json({ status: 500, data: "internal error" });
    }
});
router.get("/getusermappingsbyiduser/:id", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  // var apps = dcodeInfo.apps[0];
  let id = req.params.id;
  try {
    const allusermap = await conn
      .select(
        "id", 
        "idusrkkt", 
        "usrkktname",  
        "dbinstance", 
        "vendorid",
        "vendorname",
        "orgid",
        "idtenant",
        "targetemail1",
        "targetemail2",
        "idtenantuser",
        "created_at",
        "updated_at",
        "created_by",
        "companyname",
        "companycode"
      )
      .from("spb_usermapping")
      .where("idusrkkt", id)
      .orderBy('companyname');
    if (allusermap.length > 0) {
        res
          .status(200)
          .json({ status: 200, data: allusermap });
     
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: "Data tidak ditemukan" });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.post("/insertusermap", async (req, res, next) => {
    const authHeader = req.headers.authorization;
    var dcodeInfo = req.userData;
    try {
      // var apps = dcodeInfo.apps[0];
      const { payload, payloads } = req.body;
      const today = moment().format("YYYY-MM-DD HH:mm:ss");
      console.log(">>>>>>>>>>>>>>> PAYLOAD di /insertusermap ", payloads);
      
      if(payloads.length > 0) {
        // console.log(">>>>> PAYLOAD ", payload);
        // console.log(">>>>> PAYLOADS ", payloads);

        // >>>>> PAYLOAD  {
        //   idusrkkt: 39,
        //   usrkktname: 'AFNIMAR',
        //   dbinstance: null,
        //   vendorid: null,
        //   vendorname: null,
        //   orgid: null,
        //   idtenant: null,
        //   targetemail1: null,
        //   targetemail2: null,
        //   idtenantuser: 39,
        //   created_by: 2,
        //   companyname: null,
        //   companycode: null
        // }
        // >>>>> PAYLOADS  [
        //   {
        //     company: 'ANUGRAH ARGON MEDICA, PT',
        //     fullname: 'Afina Sinar Cemerlang, PT',
        //     idvendor: 1239227,
        //     status: 1,
        //     party_number: '445990',
        //     segment1: '3665'
        //   },
        //   {
        //     company: 'DJEMBATAN DUA, PT',
        //     fullname: 'Afina Sinar Cemerlang, PT',
        //     idvendor: 1239227,
        //     status: 1,
        //     party_number: '445990',
        //     segment1: '3665'
        //   }
        // ]
        let insertPyld = [];
        await payloads.map(async (pyld) => {
          let ptload =  {
              idusrkkt: payload.idusrkkt,
              usrkktname: payload.usrkktname,
              dbinstance: null,
              vendorid: pyld.idvendor,
              vendorname: pyld.fullname,
              orgid: null,
              idtenant: null,
              targetemail1: null,
              targetemail2: null,
              idtenantuser: payload.idusrkkt,
              created_by: payload.created_by,
              companyname: pyld.company,
              companycode: pyld.party_number
            }
            insertPyld.push(ptload);
        });
        console.log("LAST PAYLOAD ", insertPyld);
        
        const resp = await conn("spb_usermapping").insert(insertPyld);
        if (resp > 0) {
          res.status(200).json({ status: 200, data: resp });
        } else {
          res.status(500).json({
            status: 500,
            data: "Error insert m_app ",
          });
        }
      } else {
        const resp = await conn("spb_usermapping").insert(payload);
        if (resp > 0) {
          res.status(200).json({ status: 200, data: resp });
        } else {
          res.status(500).json({
            status: 500,
            data: "Error insert m_app ",
          });
        }
      }

     
    } catch (err) {
      console.log(err);
      res.status(500).json({ status: 500, data: "Error insert User" });
    }
});
router.get("/deleteusermap/:id", async (req, res, next) => {
  // const authHeader = req.headers.authorization;
  console.log("################## >>>> ##################################");
  var dcodeInfo = req.userData;
  // var apps = dcodeInfo.apps[0];
  let idpay = req.params.id;
  console.log("### User Code ### " + idpay);
  try {
    // let resp = await pool.query(
    //   "DELETE FROM m_pay where bank_code = $1",
    //   [bankcode]
    // );
    let resp = await conn("spb_usermapping").where("idusrkkt", idpay).del();
    res.status(200).json({ status: 200, data: "Success" });
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error retrive Users" });
  }
});




  module.exports = router;