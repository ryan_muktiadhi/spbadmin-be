const express = require("express"),
  app = express();
const router = express.Router();
const jwttools = require("../../routes/services/utils/encryptdecryptjwt");
const groupUtils = require("./utils/groupUtils");
const pool = require("../../connection/db");
const checkAuth = require("../../middleware/check-auth");
const moment = require("moment");
// const axios = require('axios')
router.use(checkAuth);
const knex = require("../../connection/dborm");
var conf = require("../../config.json");
const conn = knex.conn();

router.get("/getemployees", async (req, res, next) => {
    const authHeader = req.headers.authorization;
    var dcodeInfo = req.userData;
    var apps = dcodeInfo.apps[0];
    let cname = req.params.cname;
    try {
      const allEmployees = await conn
        .select(
          "id", 
            "idemployee", 
            "nikemployee",  
            "employeename", 
            "companycode",
            "companyname",
            "db_instance",
            "orgid",
            "divid",
            "phone",
            "email1",
            "email2",
            "ext",
            "created_by",
            "created_at",
            "updated_at",
            "active"
        )
        .from("spb_employee")
        // .where("NAME", cname);
      if (allEmployees.length > 0) {
          console.log("DATA ", allEmployees);
          res
            .status(200)
            .json({ status: 200, data: allEmployees });
      } else {
        setTimeout(function () {
          res.status(200).json({ status: 202, data: "Data tidak ditemukan" });
        }, 500);
      }
    } catch (err) {
      console.log(err);
      res.status(500).json({ status: 500, data: "internal error" });
    }
});
router.get("/getemployeebyCompany/:company", async (req, res, next) => {
//   const authHeader = req.headers.authorization;
  // console.log("MASUK ANJING");
  var dcodeInfo = req.userData;
  var apps = dcodeInfo.apps[0];
  let companyname = req.params.company;
  // console.log("MASUK ANJING");
  try {
    const allEmployees = await conn
      .select(
        "id", 
            "idemployee", 
            "nikemployee",  
            "employeename", 
            "companycode",
            "companyname",
            "db_instance",
            "orgid",
            "divid",
            "phone",
            "email1",
            "email2",
            "ext",
            "created_by",
            "created_at",
            "updated_at",
            "active"
      )
      .from("spb_employee")
      .where("companyname", companyname)
      .orderBy('employeename');
    if (allEmployees.length > 0) {
        // console.log("Data ",allEmployees);
        res
          .status(200)
          .json({ status: 200, data: allEmployees });
     
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: "Data tidak ditemukan" });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

 
  
  module.exports = router;