exports.up = function (knex) {
  return knex.schema
    .createTable("m_accounttype", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.string("code");
      table.string("name_cb");
      table.string("type_cb");
      table.string("name_bf");
      table.string("type_bf");
      table.integer("status").defaultTo(0);
      table.bigInteger("id_tenant").defaultTo(0);
      table.timestamp("expiredate", { useTz: false }).defaultTo(knex.fn.now());
    })
    .createTable("m_bic", (table) => {
      table.string("bic_code");
      table.string("bank_name");
      table.timestamp("last_update_date", { useTz: false });
      table.string("change_who");
      table.bigInteger("idtenant").defaultTo(0);
      table
        .timestamp("created_date", { useTz: false })
        .defaultTo(knex.fn.now());
      table.string("bank_code");
      table.integer("status").defaultTo(0);
    })
    .createTable("m_branch", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.string("citycode");
      table.string("cityname");
      table.string("branchcode");
      table.string("branchname");
      table.integer("status").defaultTo(0);
      table.timestamp("last_update_date", { useTz: false });
      table.string("change_who");
      table
        .timestamp("created_date", { useTz: false })
        .defaultTo(knex.fn.now());
      table.string("bank_code");
      table.bigInteger("idtenant").defaultTo(0);
    })
    .createTable("m_channeltype", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.string("channeltype");
      table.integer("status").defaultTo(0);
      table
        .timestamp("created_date", { useTz: false })
        .defaultTo(knex.fn.now());
      table.string("change_who");
      table.timestamp("last_update_date", { useTz: false });
      table.bigInteger("idtenant").defaultTo(0);
      table.string("channelcode");
    })
    .createTable("m_customertype", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.string("code");
      table.string("name");
      table.string("type_cb");
      table.string("type_bf");
      table.integer("status").defaultTo(0);
      table.bigInteger("id_tenant").defaultTo(0);
      table
        .timestamp("created_date", { useTz: false })
        .defaultTo(knex.fn.now());
    })
    .createTable("m_datareport", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.string("headercol");
      table.string("datacol");
      table
        .timestamp("created_date", { useTz: false })
        .defaultTo(knex.fn.now());
      table.string("srcpayload");
      table.string("filename");
    })
    .createTable("m_idtype", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.string("code");
      table.string("name");
      table.string("type_cb");
      table.string("type_bf");
      table.integer("status").defaultTo(0);
      table.bigInteger("idtenant").defaultTo(0);
      table
        .timestamp("created_date", { useTz: false })
        .defaultTo(knex.fn.now());
    })
    .createTable("m_limit", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.string("limit_name");
      table.string("tran_type");
      table.string("min_tran");
      table.string("max_limit");
      table.integer("status").defaultTo(0);
      table.bigInteger("idtenant").defaultTo(0);
      table
        .timestamp("created_date", { useTz: false })
        .defaultTo(knex.fn.now());
    })
    .createTable("m_prefund", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.bigInteger("idparticipant");
      table.integer("amount");
      table.timestamp("last_update_date", { useTz: false });
      table.string("change_who");
      table.bigInteger("idtenant").defaultTo(0);
      table
        .timestamp("created_date", { useTz: false })
        .defaultTo(knex.fn.now());
    })
    .createTable("m_proxy", (table) => {
      table.string("registration_id");
      table.string("proxy_type");
      table.string("proxy_value");
      table.string("registrar_bic");
      table.string("account_number");
      table.string("account_type");
      table.string("account_name");
      table.string("identification_number_type");
      table.string("identification_number_value");
      table.string("proxy_status");
      table.timestamp("last_update_date", { useTz: false });
      table
        .timestamp("created_date", { useTz: false })
        .defaultTo(knex.fn.now());
      table.string("change_who");
      table.bigInteger("idtenant").defaultTo(0);
    })
    .createTable("m_proxyalias", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.string("name");
      table.string("accnum");
      table.string("prxaddress");
      table.string("branch");
      table.string("channel");
      table.string("proxytype");
      table.string("secondid");
      table.string("custresident");
      table
        .timestamp("created_date", { useTz: false })
        .defaultTo(knex.fn.now());
      table.string("change_who");
      table.timestamp("last_update_date", { useTz: false });
      table.bigInteger("idtenant").defaultTo(0);
      table.integer("stsprxadd");
      table.string("secondtype");
      table.string("proxystat");
    })
    .createTable("m_proxytype", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.string("proxycode").notNullable();
      table.string("proxyname").notNullable();
      table.integer("status").defaultTo(0);
      table
        .timestamp("created_date", { useTz: false })
        .defaultTo(knex.fn.now());
      table.integer("change_who");
      table
        .timestamp("last_update_date", { useTz: false })
        .defaultTo(knex.fn.now());
      table.integer("idtenant").defaultTo(0);
    })
    .createTable("m_resident", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.string("code").notNullable();
      table.string("name").notNullable();
      table.string("status_cb").notNullable();
      table.string("status_bf").notNullable();
      table.integer("status");
      table.bigInteger("id_tenant");
      table
        .timestamp("created_date", { useTz: false })
        .defaultTo(knex.fn.now());
    })
    .createTable("m_systemparam", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.string("paramname").notNullable();
      table.string("paramvalua").notNullable();
      table.integer("status").defaultTo(0);
      table
        .timestamp("created_date", { useTz: false })
        .defaultTo(knex.fn.now());
      table.bigInteger("change_who");
      table.timestamp("last_update_date", { useTz: false });
      table.bigInteger("id_tenant").defaultTo(0);
    })
    .createTable("m_trxcost", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.string("fee");
      table.integer("values");
      table.integer("status").defaultTo(0);
      table.timestamp("last_update_date", { useTz: false });
      table.string("change_who");
      table.bigInteger("id_tenant").defaultTo(0);
      table
        .timestamp("created_date", { useTz: false })
        .defaultTo(knex.fn.now());
    })
    .createTable("outbound_message", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.string("bizmsgid");
      table.string("channel");
      table.timestamp("chnl_req_time", { useTz: false });
      table.timestamp("chnl_resp_time", { useTz: false });
      table.timestamp("cihub_req_time", { useTz: false });
      table.timestamp("cihub_resp_time", { useTz: false });
      table.text("error_msg");
      table.text("full_request_msg");
      table.text("full_response_msg");
      table.string("intrn_ref_id");
      table.string("message_name");
      table.string("recpt_bank");
      table.string("resp_bizmsgid");
      table.string("resp_status");
      table.string("rejct_msg");
      table.integer("saf_counter");
    })
    .createTable("prefund_dashboard", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.bigInteger("min");
      table.bigInteger("max");
      table.bigInteger("current");
    })
    .createTable("prefund_history", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.string("refNum");
      table.integer("transactionType");
      table.string("prefundAmount");
      table.timestamp("datetime", { useTz: false });
    })
    //ganti ke sys_param ke db baru
    //.createTable("system_param", (table) => {
    //  table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
    //  table.string("param_name").notNullable;
    //  table.string("param_value").notNullable;
    //  table.string("category");
    //  table.string("description");
    //})
    .createTable("trx_log", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.string("status");
      table.string("trxnumber");
      table.string("refnumbifast");
      table.string("channel");
      table.string("destbank");
      table.string("srcbank");
      table.string("destaccnum");
      table.string("srcaccnum");
      table.string("destaccname");
      table.string("srcaccname");
      table
        .timestamp("created_date", { useTz: false })
        .defaultTo(knex.fn.now());
      table.string("change_who");
      table.timestamp("last_updated_date", { useTz: false });
      table.decimal("amount", 12, 1).defaultTo(0.0);
      table.integer("transacttype").defaultTo(0);
      table.integer("idtenant");
      table.string("branchcode");
      table.string("feecode");
      table.string("feeamount");
    })
    .createTable("t_proxy_mgmt", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.string("komi_unique_id");
      table.string("bifast_trx_no");
      table.string("komi_trx_no");
      table.string("account_name");
      table.string("account_number");
      table.string("account_type");
      table.string("call_status");
      table.string("chnl_no_ref");
      table.bigInteger("cihub_elapsed_time");
      table.string("customer_id");
      table.string("customer_type");
      table.string("display_name");
      table.string("komi_trns_id");
      table.string("operation_type");
      table.string("proxy_type");
      table.string("proxy_value");
      table.timestamp("request_dt", { useTz: false });
      table.string("resident_status");
      table.string("resp_status");
      table.string("scnd_id_type",);
      table.string("scnd_value");
      table.string("town_name");
      table.string("branch");
      table.string("channel");
      table.bigInteger("trx_duration");
      table.string("trx_reason_code");
      table.string("trx_reason_message");
      table.string("sender_bank");
      table.string("trx_type");
    })
      .createTable("t_transaction", (table) => {
        table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
        table.string("komi_unique_id");
        table.string("bifast_trx_no");
        table.string("komi_trx_no");
        table.string("channel_type");
        table.string("branch_code");
        table.string("recipient_bank");
        table.string("sender_bank");
        table.string("recipient_account_no");
        table.string("recipient_proxy_type");
        table.string("recipient_proxy_alias");
        table.string("recipient_account_name");
        table.string("sender_account_no");
        table.string("sender_account_name");
        table.string("charge_type");
        table.string("trx_type");
        table.bigInteger("trx_amount");
        table.bigInteger("trx_fee");
        table.timestamp("trx_initiation_date", { useTz: false });
        table.string("trx_status_code",);
        table.string("trx_status_message");
        table.string("trx_response_code");
        table.string("trx_reason_code");
        table.string("trx_reason_message");
        table.string("trx_proxy_flag");
        table.string("trx_SLA_flag");
        table.bigInteger("trx_duration");
        table.timestamp("trx_complete_date", { useTz: false });
        table.string("trx_info01");
        table.string("trx_info02");
        table.string("trx_info03");
        table.string("trx_info04");
        table.string("trx_info05");
      })
      .createTable("kc_channel", (table) => {
        table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
        table.string("channel_id");
        table.string("channel_name");
        table.string("channel_type");
        table.string("marchant_code");
        table.timestamp("create_dt", { useTz: false })
            .defaultTo(knex.fn.now());
        table.timestamp("modif_dt", { useTz: false });
        table.string("secret_key");
      })
    .createTable("trx_reportlog", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.integer("rpttype").defaultTo(0);
      table.integer("status").defaultTo(0);
      table.text("description");
      table.text("pathfile");
      table
        .timestamp("created_date", { useTz: false })
        .defaultTo(knex.fn.now());
      table.bigInteger("idtenant").defaultTo(0);
      table.string("change_who");
      table.timestamp("last_update_date", { useTz: false });
    });
};

exports.down = function (knex) {
  return knex.schema
    .dropTable("m_accounttype")
    .dropTable("m_bic")
    .dropTable("m_branch")
    .dropTable("m_channeltype")
    .dropTable("m_customertype")
    .dropTable("m_datareport")
    .dropTable("m_idtype")
    .dropTable("m_limit")
    .dropTable("m_prefund")
    .dropTable("m_proxy")
    .dropTable("m_proxyalias")
    .dropTable("m_proxytype")
    .dropTable("m_resident")
    .dropTable("m_systemparam")
    .dropTable("m_trxcost")
    .dropTable("outbound_message")
    .dropTable("prefund_dashboard")
    .dropTable("prefund_history")
    .dropTable("system_param")
    .dropTable("trx_log")
    .dropTable("t_transaction")
    .dropTable("trx_reportlog")
    .dropTable("t_proxy_mgmt");
};
